<?php
    include ('vues/common/doctype.php');
    include ('vues/common/head.php');
    include ('vues/common/open_body.php');
    include ('vues/common/open_header.php');
    include ('vues/common/social_networks.php');
    include ('vues/common/menu.php');
    include ('vues/common/close_header.php');
    
    
    include ('vues/rubriques/home/home_slider.php');
    include ('vues/rubriques/home/features.php');
    include ('vues/rubriques/home/actions.php');
    include ('vues/rubriques/home/services.php');
    include ('vues/rubriques/home/clients.php');
    include ('vues/common/footer.php');
    include ('vues/common/scripts.php');
    include ('vues/common/close_body.php');
?>